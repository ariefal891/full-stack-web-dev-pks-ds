<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post_id = $request->post_id;
        $comments =  Comment::where('post_id', $post_id)->latest()->get();
        return response()->json([
            'success' => true,
            'masssage' => 'Data daftar Comments berhasil ditampilkan',
            'data' => $comments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll =  $request->all();

        $validator = Validator::make($requestAll, [
           'content' => 'required',
           'post_id' => 'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400,);
        }
        $comment = Comment::create([
            'content' => $request->content,
            'post_id' => $request->post_id,
        ]);
        // $comment = Comment::create($requestAll);
        if($comment){
            return response()->json([
                'success' => true,
                'masssage' => 'Data  Comment berhasil ditambahkan',
                'data' => $comment
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Commetn Gagal Di buat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::find($id);

        if($comment){
            return response()->json([
                'success' => true,
                'message' => 'Data comment berhasil ditampilkan',
                'data' => $comment
            ], 200);
        }
        return response()->json()([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ],404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestAll =  $request->all();

        $validator = Validator::make($requestAll, [
           'content' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400,);
        }
        $comment = Comment::find($id);

        if($comment){
            $comment->update([
                'content' => $request->content,
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Data dengan comment berhasil di update',
                'data' => $comment
            ], 200);
        }
        return response()->json()([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ],404);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);

        if($comment){
            $comment->delete();
            return response()->json([
                'success' => true,
                'message' => 'Data  comment berhasil di delete',
                'data' => $comment
            ], 200);
        }
        return response()->json()([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ],404);
    }
}
