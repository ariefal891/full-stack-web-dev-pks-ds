<?php

namespace App\Http\Controllers;

use App\roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles =  roles::latest()->get();
        return response()->json([
            'success' => true,
            'masssage' => 'Data daftar Role berhasil ditampilkan',
            'data' => $roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll =  $request->all();

        $validator = Validator::make($requestAll, [
           'nama' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400,);
        }
        $role = roles::create([
            'nama' => $request->nama,
        ]);
        // $post = roles::create($requestAll);
        if($role){
            return response()->json([
                'success' => true,
                'masssage' => 'Data  Role berhasil ditambahkan',
                'data' => $role
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data Role Gagal Di buat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = roles::find($id);

        if($role){
            return response()->json([
                'success' => true,
                'message' => 'Data role berhasil ditampilkan',
                'data' => $role
            ], 200);
        }
        return response()->json()([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ],404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestAll =  $request->all();

        $validator = Validator::make($requestAll, [
           'nama' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400,);
        }
        $role = roles::find($id);

        if($role){
            $role->update([
                'nama' => $request->nama,
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Data dengan role berhasil di update',
                'data' => $role
            ], 200);
        }
        return response()->json()([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ],404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = roles::find($id);

        if($role){
            $role->delete();
            return response()->json([
                'success' => true,
                'message' => 'Data  Role berhasil di delete',
                'data' => $role
            ], 200);
        }
        return response()->json()([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ],404);
    }
}
