<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
    protected $fillable = ['nama'];
    protected $keyType = 'string';
    public $incrementing = false;


    protected static function boot (){
        parent::boot();
        
        static::creating(function($md){
            if(empty ($md->{$md->getKeyName()})){
                $md->{$md->getKeyName()} = Str::uuid();
            }
        } );

    }
}
