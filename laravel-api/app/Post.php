<?php

namespace App;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    Use Uuid;
    public $increment = false;
    protected $keyType = "uuid";
    protected  $fillable = ['title'];
    
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    } 

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

}