<?php


abstract class Hewan
{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    public function atraksi(){
        return $this->nama. " sedang" . $this->keahlian;
    }
    
}


trait Fight
{
    public $attackPower;
    public $defencePower;
    public function serang($hewanDiserang){
        echo $this->nama." sedang menyerang ".$hewanDiserang->nama."<br > ";
        $hewanDiserang->diserang($this->attackPower);
        
    }
    // todo
    public function diserang($attackPower){
        
        $this->darah = $this->darah - ($attackPower - $this->defencePower);
        echo $this->nama." sedang diserang "."<br> <br>";
    }
}


class Elang extends Hewan
{
    use Fight;
   
    function __construct($nama, $jumlahKaki, $keahlian, $attackPower , $defencePower) {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower; 

      }
    // todo belum ada jenis hewan, termasuk semua properdi di class lain
    public function getInfoHewan()
    {
        echo "Nama : " . $this->nama . "<br>" .
            "Jumlah Kaki : " . $this->jumlahKaki  . "<br>" .
            "Keahlian  : " . $this->keahlian . "<br>" .
            "Darah : " . $this->darah . "<br>" . 
            "Attack Power : " . $this->attackPower  . "<br>" . 
            "Deffence Power : " . $this->defencePower . "<br>" . 
            "Jenis Hewan : Elang <br> <br>" ;
    }
}
class Harimau extends Hewan 
{
    use Fight;
   
    function __construct($nama, $jumlahKaki, $keahlian, $attackPower , $defencePower) {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower; 

      }
    // todo belum ada jenis hewan, termasuk semua properdi di class lain
    public function getInfoHewan()
    {
        echo "Nama : " . $this->nama . "<br>" .
            "Jumlah Kaki : " . $this->jumlahKaki  . "<br>" .
            "Keahlian  : " . $this->keahlian . "<br>" .
            "Darah : " . $this->darah . "<br>" . 
            "Attack Power : " . $this->attackPower  . "<br>" . 
            "Deffence Power : " . $this->defencePower . "<br>" . 
            "Jenis Hewan : Harimau <br> <br>" ;
    }
}


$elang = new Elang($nama="Elang", $jumlahKaki=2, $keahlian="Terbang tinggi", $attackPower=10, $defencePower=5);
$harimau =  new Harimau("Harimau", 2, "Lari Cepat", 7, 8);
$elang->getInfoHewan();
$harimau->getInfoHewan();
$elang->serang($harimau);
$harimau->getInfoHewan();
$harimau->serang($elang);
$elang->getInfoHewan();
// get getInfoHewan dua duanay
// $elang->serang($harimau);
// getinfo harimau


// $elang->setHewan("Elang", 2, "Terbang Tinggi");
// echo $elang->getHewan();
// $elang->setAttack(10);
// $elang->setDeffence(5);
// echo PHP_EOL;
// $harimau = new Harimau();
// $harimau->setHewan("Harimau", 4, "Lari Cepat", 7);
// echo $harimau->getHewan();
// $harimau->setAttack(7);
// $harimau->setDeffence(8);
// echo PHP_EOL;


